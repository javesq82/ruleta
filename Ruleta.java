import javax.swing.JOptionPane;
import java.util.Random;

public class Ruleta extends Revolver {
	
	private int partida;
	private int nivelDificultad;
	private boolean nuevaPartida;
	private Jugador jugador1;
	
	public Ruleta () {
		super();
		this.partida= partida;
		this.nivelDificultad= nivelDificultad;
		this.nuevaPartida= nuevaPartida;
		jugador1= new Jugador();
	}
	
	
	public int getPartida() {
		return partida;
	}

	public void setPartida(int partida) {
		this.partida = partida;
	}

	public int getNivelDificultad() {
		return nivelDificultad;
	}

	public void setNivelDificultad(int nivelDificultad) {
		this.nivelDificultad = nivelDificultad;
	}

	public boolean isNuevaPartida() {
		return nuevaPartida;
	}

	public void setNuevaPartida(boolean nuevaPartida) {
		this.nuevaPartida = nuevaPartida;
	}
	

	public int jugar(Jugador jugador1) {
		Random r = new Random();
		jugador1.getPartidasGanadas();
		jugador1.getPartidasPerdidasPorDisparo();
		jugador1.getPartidasPerdidasPorAbandono();
		
		int facil = r.nextInt(6);
		int intermedio=r.nextInt(8);
		int dificil= r.nextInt(10);
		nivelDificultad = Integer.parseInt(JOptionPane.showInputDialog("Ingresa nivel de dificultad: facil(1), intermedio(2), dificil(3): "));
		
		if(nivelDificultad==1) {
			disparo= Integer.parseInt(JOptionPane.showInputDialog("Deseas hacer el disparo? si(1) no(2): "));
				if(disparo==1) { 
					if(disparo==facil) {
						jugador1.setPartidasPerdidasPorDisparo(1);
						System.out.println("haz perdido");
						
						
					}else {
						return disparo;
					}
				}if(disparo==2) {
					if(disparo==facil) {
						jugador1.setPartidasGanadas(1);
						System.out.println("haz ganado!");
					}else {
						jugador1.setPartidasPerdidasPorAbandono(1);
						System.out.println("haz perdido");
					}
				}
				
				
			
			
		}if(nivelDificultad==2) {
			disparo= Integer.parseInt(JOptionPane.showInputDialog("Deseas hacer el disparo? si(1) no(2): "));
			if(disparo==1) { 
				if(disparo==intermedio) {
					jugador1.setPartidasPerdidasPorDisparo(1);
					System.out.println("haz perdido");
					
					
				}else {
					return disparo;
				}
			}if(disparo==2) {
				if(disparo==intermedio) {
					jugador1.setPartidasGanadas(1);
					System.out.println("haz ganado!");
				}else {
					jugador1.setPartidasPerdidasPorAbandono(1);
					System.out.println("haz perdido");
				}
			}
		
	    }
		if(nivelDificultad==3) {
			disparo= Integer.parseInt(JOptionPane.showInputDialog("Deseas hacer el disparo? si(1) no(2): "));
				if(disparo==1) { 
					if(disparo==dificil) {
						jugador1.setPartidasPerdidasPorDisparo(1);
						System.out.println("haz perdido");
						
						
					}else {
						return disparo;
					}
				}if(disparo==2) {
					if(disparo==dificil) {
						jugador1.setPartidasGanadas(1);
						System.out.println("haz ganado!");
					}else {
						jugador1.setPartidasPerdidasPorAbandono(1);
						System.out.println("haz perdido");
					}
				}
	
       }
		return nivelDificultad;


	}	
	
}	


	
	




public class Jugador {
	
	private int partidasGanadas;
	private int partidasPerdidasPorDisparo;
	private int partidasPerdidasPorAbandono;

	
	public Jugador() {
		this.partidasGanadas= 0;
		this.partidasPerdidasPorDisparo=0;
		this.partidasPerdidasPorAbandono=0;
	}


	public int getPartidasGanadas() {
		return partidasGanadas;
	}

	public void setPartidasGanadas(int partidasGanadas) {
		this.partidasGanadas = partidasGanadas;
	}


	public int getPartidasPerdidasPorDisparo() {
		return partidasPerdidasPorDisparo;
	}


	public void setPartidasPerdidasPorDisparo(int partidasPerdidasPorDisparo) {
		this.partidasPerdidasPorDisparo = partidasPerdidasPorDisparo;
	}


	public int getPartidasPerdidasPorAbandono() {
		return partidasPerdidasPorAbandono;
	}


	public void setPartidasPerdidasPorAbandono(int partidasPerdidasPorAbandono) {
		this.partidasPerdidasPorAbandono = partidasPerdidasPorAbandono;
	}



}
